
> Here's some sqlite statements, we are just scratching the surface there are more out there.

- `CREATE TABLE`
```sql
CREATE TABLE "movies" (
	"movie id" INTEGER,
	"movie name" TEXT NOT NULL,
	"genres" TEXT NOT NULL,
	PRIMARY KEY("movie id")
);
```

- `ALTER TABLE
```sql
-- add a column to an existing table
ALTER TABLE "movies" ADD COLUMN "origin" TEXT;

-- rename a column 
ALTER TABLE "movies" RENAME COLUMN "movie id" TO "id";

-- rename a table 
ALTER TABLE "movies" RENAME TO "my movies";
```

- `DROP TABLE`
```sql
DROP TABLE "my movies";
```

- `INSERT INTO`
```sql
INSERT INTO "my movies"
("movie name", "genres", "origin")
VALUES ("hangover", "comedy", "united states");
```

- `SELECT`
```sql
SELECT * FROM "my movies";

SELECT "id", "movie name" FROM "my movies";
```

- `WHERE`
```sql
SELECT * FROM "my movies" WHERE "genres"="comedy" AND "origin"="united states";
```

- `UPDATE`
```sql
UPDATE "my movies" SET "genres" = "war, adventure, action" WHERE "movie name" = "game of thrones";
```

- `DELETE FROM`
```sql
DELETE FROM "my movies" WHERE "movie name" = "vikings";
```

- `LIKE`
```sql
DELETE FROM "my movies" WHERE "movie name" LIKE "the office" OR "movie name" LIKE "friends";

UPDATE "my movies" SET "genres" = "comedy" WHERE "movie name" LIKE "how i met your mother";

SELECT * FROM "my movies" WHERE "movie name" LIKE "%office";
```

- `LIMIT`
```sql
SELECT * FROM "my movies" LIMIT 10; 

-- display 15 rows from top after skipping 5 rows
SELECT * FROM "my movies" LIMIT 15 OFFSET 5;
```

- `ORDER BY`
```sql
SELECT * FROM "my movies" ORDER BY "movie name" LIMIT 20;

-- display rows order by movie name in descending order
SELECT * FROM "my movies" ORDER BY "movie name" DESC LIMIT 20;
```

- `GROUP BY`
```sql
SELECT * FROM "my movies" GROUP BY "movie name";
```

- `DISTINCT`
```sql
SELECT DISTINCT("movie name") FROM "my movie";

SELECT DISTINCT(UPPER("movie name")) FROM "my movie";
```

- `COUNT`
```sql
SELECT COUNT(DISTINCT("movie name")) FROM "my movie";
```

- `AS`
```sql
SELECT COUNT(*) AS "counter" FROM "my movies" WHERE "movie name" LIKE "the office";
```

- `INDEX`
> In SQL, an index is a data structure that allows faster retrieval of rows from a table. An index creates an entry for each value that appears in the indexed column(s) of the table and provides a pointer to the corresponding row. This makes it faster to search for specific rows, especially if the table is large.
>You can create an index on one or more columns of a table. There are two main types of indexes: unique indexes and non-unique indexes. A unique index ensures that no two rows in the table have the same indexed values, while a non-unique index does not enforce this constraint.
```sql
CREATE INDEX index_name ON table_name (column1, column2);

CREATE UNIQUE INDEX index_name ON table_name (column1, column2);

CREATE INDEX "movie name index" ON "my movies" ("movie name");
```


- GROUPING MULTIPLE QUERIES
```sql
SELECT DISTINCT(title) FROM shows WHERE id IN (SELECT show_id FROM shows WHERE genre = "comedy") ORDER BY;
```
