
## SQLite Commands

> Some of important commands are:

- `mode <mode name>` 
> To spacify how do we want to display our output. `<mode name>` could be ***csv, html, column, list, line, tcl, tabs***

- `.import <file name> <table name>`
> Import a file, create a new table and populate the table using file content.

- `.schema`
> Display schemas (tables along with their fields and their types) of that database.

- `.header <on/off>`
> Do you want to display headers (field names) in your output ?

- `.timer <on/off>`


---

## SQLite Clauses

> Clauses are keywords in SQLite that can do certen task. Here are some clauses:

-  `SELECT`
> specifies the columns to be retrieved from the database.

- `FROM`
> specifies the table(s) from which the data is to be retrieved.

- `WHERE`
> specifies a condition that rows must meet to be included in the result set.

- `GROUP BY`
> groups the results by a specified column or expression.

- `HAVING`
> applies a condition to the grouped results.

- `ORDER BY`
> sorts the results by a specified column or expression.

- `LIMIT`
> limits the number of rows returned in the result set.

- `OFFSET`
> skips a specified number of rows in the result set before returning the remaining rows.

- `DISTINCT`
> The DISTINCT clause is used to eliminate duplicate rows from the result set.

- `AS`
> The AS clause is used to give a column or expression a temporary name.

- `INNER JOIN`
> The INNER JOIN clause is used to retrieve rows from two or more tables that have matching values in a specified column

- `LEFT JOIN`
> The LEFT JOIN clause is similar to INNER JOIN, but it returns all rows from the left table (the first table specified in the FROM clause), even if there are no matching rows in the right table.

- `RIGHT JOIN`
> The RIGHT JOIN clause is similar to INNER JOIN, but it returns all rows from the right table (the second table specified in the FROM clause), even if there are no matching rows in the left table.

- `FULL JOIN`
> The FULL JOIN clause is similar to INNER JOIN, but it returns all rows from both tables, regardless of whether there are matching values in the joined column.

- `UNION`
> The UNION clause is used to combine the results of two or more SELECT statements into a single result set.

- `CASE`
> The CASE clause is used to create conditional statements within a SELECT statement.

- `BETWEEN`
> The BETWEEN operator is used to select values within a given range.

- `LIKE`
> The LIKE operator is used to match values using a pattern.

- `IN`
> The IN operator is used to match values against a list of values.

- `IS NULL`
> The IS NULL operator is used to select values that are NULL (i.e., have no value).

- `EXISTS`
> The EXISTS operator is used to test for the existence of rows in a subquery.

- `CREATE TABLE`
> The CREATE TABLE clause is used to create a new table in the database.

- `ALTER TABLE`
> The ALTER TABLE clause is used to modify the structure of an existing table.

- `DROP TABLE`
> The DROP TABLE clause is used to delete a table from the database.

- `INSERT INTO`
> The INSERT INTO clause is used to insert a new row into a table.

- `UPDATE`
> The UPDATE clause is used to modify the values of one or more columns in a table.

- `DELETE FROM`
> The DELETE FROM clause is used to delete rows from a table.

- `INDEX`
> The INDEX clause is used to create an index on a table.

- `UNIQUE`
> The UNIQUE keyword is used to create a unique constraint on a table, which means that the values in a specific column or set of columns must be unique across all rows in the table.

- `PRIMARY KEY`
> The PRIMARY KEY keyword is used to create a primary key on a table. A primary key is a column or set of columns that uniquely identifies each row in the table.

- `FOREIGN KEY`
> The FOREIGN KEY keyword is used to create a foreign key on a table. A foreign key is a column or set of columns that references the primary key of another table.

- `CHECK`
> The CHECK keyword is used to add a check constraint to a table. A check constraint allows you to specify a condition that must be met by the values in a specific column or set of columns.


---

## SQLite Datatypes

- `INTEGER`
- `TEXT`
- `NULL`
- `REAL`
- `BLOB`
>In SQLite, a BLOB (short for "Binary Large OBject") is a data type that is used to store large binary objects, such as images or other types of multimedia. A BLOB is typically stored as a sequence of bytes, and it can be of any size up to the maximum size of a database.


---

## SQLite Operators

#### Logical Operators
- `AND`
- `OR`
- `NOT`

#### Comparison Operators
- `=`
- `!=`
- `<`
- `>`
- `<=`
- `>=`

#### Arithmetic Operators
- `+`
- `-`
- `*`
- `/`
- `%`

#### Bitwise Operators
- `&`
- `|`
- `~`
- `<<`
- `>>`