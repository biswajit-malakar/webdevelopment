
## Array creation
>There are several ways to create arrays in JavaScript.

### Way #1
>The most common way to create an array is to use the `Array` constructor. You can pass the elements of the array as arguments to the constructor, or you can pass a single argument that specifies the length of the array.
```javascript
// Create an array with three elements
const fruits = new Array('apple', 'banana', 'orange');
console.log(fruits); // Output: ['apple', 'banana', 'orange']

// Create an empty array with a length of 5
const emptyArray = new Array(5);
console.log(emptyArray); // Output: [, , , , ]
```

### Way #2
>Another way to create an array is to use an array literal, which is a list of elements surrounded by square brackets.
```javascript
const vegetables = ['tomato', 'potato', 'carrot'];
console.log(vegetables); // Output: ['tomato', 'potato', 'carrot']
console.log(vegetables[1]); // Output : potato
```

### Way #3
>Finally, you can create an array using the `Array.of()` method, which creates an array from a variable number of arguments.
```javascript
const numbers = Array.of(1, 2, 3, 4, 5);
console.log(numbers); // Output: [1, 2, 3, 4, 5]
console.log(numbers[3]); // Output : 4
```

---

## Array properties & functions

- **length** 
>In JavaScript, the `length` property is a property of the `Array` object that specifies the number of elements in the array. It is a read-only property, which means that you cannot directly modify it, but you can change the length of the array by adding or removing elements from it.
```javascript
const fruits = ['apple', 'banana', 'orange'];

console.log(fruits.length); // Output: 3

fruits.push('mango');
console.log(fruits.length); // Output: 4

fruits.pop();
console.log(fruits.length); // Output: 3
```

- **constructor**
>In JavaScript, the `constructor` property is a property of an object that refers to the function that was used to create the object. It is a read-only property that is inherited from the object's prototype, and it cannot be directly modified.
>The `constructor` property is often used to check the type of an object at runtime.
```javascript
function Animal(name) {
  this.name = name;
}

const dog = new Animal('dog');

console.log(dog.constructor === Animal); // Output: true
console.log(dog.constructor === Object); // Output: false
```

- **concat()**
>In JavaScript, the `concat()` method is a method of the `Array` object that is used to concatenate two or more arrays. It creates a new array that contains the elements of the original arrays, and it does not modify the original arrays.
```javascript
const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = ['g', 'h', 'i'];

const concatenatedArray = array1.concat(array2, array3);

console.log(concatenatedArray); // Output: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
```

- **join()**
>In JavaScript, the `join()` method is a method of the `Array` object that is used to convert the elements of an array to strings and join them together, separated by a specified delimiter.
```javascript
const fruits = ['apple', 'banana', 'orange'];

const fruitsString = fruits.join(', ');

console.log(fruitsString); // Output: "apple, banana, orange"
```

- **pop()**
>In JavaScript, the `pop()` method is a method of the `Array` object that is used to remove the last element from an array and return it. It modifies the original array, and it changes the length of the array.
```javascript
const fruits = ['apple', 'banana', 'orange'];

const lastFruit = fruits.pop();

console.log(fruits); // Output: ['apple', 'banana']
console.log(lastFruit); // Output: 'orange'
```

- **push()**
>In JavaScript, the `push()` method is a method of the `Array` object that is used to add one or more elements to the end of an array and return the new length of the array. It modifies the original array.
```javascript
const fruits = ['apple', 'banana'];

const newLength = fruits.push('orange', 'mango');

console.log(fruits); // Output: ['apple', 'banana', 'orange', 'mango']
console.log(newLength); // Output: 4
```

- **reverse()**
>In JavaScript, the `reverse()` method is a method of the `Array` object that is used to reverse the order of the elements in an array. It modifies the original array and does not create a new array.
```javascript
const numbers = [1, 2, 3, 4, 5];

numbers.reverse();

console.log(numbers); // Output: [5, 4, 3, 2, 1]
```

- **shift()**
>In JavaScript, the `shift()` method is a method of the `Array` object that is used to remove the first element from an array and return it. It modifies the original array and changes the length of the array.
```javascript
const fruits = ['apple', 'banana', 'orange'];

const firstFruit = fruits.shift();

console.log(fruits); // Output: ['banana', 'orange']
console.log(firstFruit); // Output: 'apple'
```

- **slice()**
>In JavaScript, the `slice()` method is a method of the `Array` object that is used to extract a section of an array and return a new array. It does not modify the original array.
>The `slice()` method takes two arguments: the start index and the end index of the section to extract. It includes the element at the start index and excludes the element at the end index.
```javascript
const fruits = ['apple', 'banana', 'orange', 'mango', 'kiwi'];

const slicedFruits = fruits.slice(1, 4);

console.log(slicedFruits); // Output: ['banana', 'orange', 'mango']
```

- **sort()**
>In JavaScript, the `sort()` method is a method of the `Array` object that is used to sort the elements of an array in place (i.e., it modifies the original array). The default sort order is built upon converting the elements into strings, then comparing their sequences of UTF-16 code units values.
```javascript
const numbers = [5, 3, 2, 1, 4];

numbers.sort();

console.log(numbers); // Output: [1, 2, 3, 4, 5]
```

- **toString()**
>The `toString()` method is used to convert the elements of an array to strings and join them together, separated by commas. It returns a string representation of the array.
```javascript
const fruits = ['apple', 'banana', 'orange'];

const fruitsString = fruits.toString();

console.log(fruitsString);
```

- **indexOf**
>The `indexOf()` method is used to find the first occurrence of an element in an array, and it returns the index of the element. If the element is not found, it returns -1.
```javascript
const fruits = ['apple', 'banana', 'orange', 'mango'];

console.log(fruits.indexOf('banana')); // Output: 1
console.log(fruits.indexOf('kiwi')); // Output: -1
```

- **lastIndexOf()**
>The `lastIndexOf()` method is used to find the last occurrence of an element in an array, and it returns the index of the element. If the element is not found, it returns -1.
```javascript
const fruits = ['apple', 'banana', 'orange', 'mango', 'banana'];

console.log(fruits.lastIndexOf('banana')); // Output: 4
console.log(fruits.lastIndexOf('kiwi')); // Output: -1
```

- **every()**
>The `every()` method is used to test whether all elements in an array pass a test specified by a callback function. It returns a boolean value.
```javascript
const numbers = [1, 2, 3, 4, 5];

const areAllPositive = numbers.every(function(element) {
  return element > 0;
});

console.log(areAllPositive); // Output: true
```

- **findIndex()**
>The `findIndex()` method is used to find the index of the first element in an array that satisfies a test specified by a callback function, and it returns the index of the element. If the element is not found, it returns -1.
```javascript
const numbers = [1, 2, 3, 4, 5];

const firstEvenIndex = numbers.findIndex(function(element) {
  return element % 2 === 0;
});

console.log(firstEvenIndex); // Output: 1
```