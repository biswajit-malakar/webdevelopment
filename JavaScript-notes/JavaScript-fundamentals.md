> JavaScript is a programming language that is widely used on the web. It is a client-side scripting language, which means that it is executed by the web browser on the user's computer rather than on the web server. JavaScript is primarily used to add interactivity and dynamic effects to web pages, but it can also be used for building web servers and mobile applications. It is a versatile and powerful language that is easy to learn and use.

## `let` v/s `var` keywords
>In JavaScript, `var` and `let` are both used to declare variables, but they have some differences in their behavior:

1.  **Scope**: The scope of a `var` variable is the entire enclosing function, while the scope of a `let` variable is the block in which it is defined, as well as any sub-blocks. This means that a `let` variable is only accessible within the block of code in which it is defined, while a `var` variable is accessible throughout the function in which it is defined.    
2.  **Hoisting**: `var` variables are hoisted to the top of their scope, which means that they can be accessed before they are declared. `let` variables are not hoisted, so they cannot be accessed before they are declared.
3.  **Redeclaration**: It is not possible to redeclare a `let` variable with the same name in the same block of code, but it is possible to do so with `var`.

>In general, it is recommended to use `let` instead of `var` whenever possible, because `let` has a more intuitive behavior and helps to prevent certain types of bugs that can occur with `var`. However we can also declare a variable without using any keyword for that case javascript will make that variabe scope as global.
>Keep in mind that javascript always prefer local variables more than global variables.

----

## Operators

In JavaScript, there are several types of operators that can be used to perform different kinds of operations:

1.  **Arithmetic operators**: These operators perform basic arithmetic operations, such as addition, subtraction, multiplication, and division.
```javascript
let x = 10;
let y = 5;
console.log(x + y);  // Output: 15
console.log(x - y);  // Output: 5
console.log(x * y);  // Output: 50
console.log(x / y);  // Output: 2
console.log(x % y);  // Output: 0
```
1.  **Assignment operators**: These operators are used to assign a value to a variable.
```javascript
let x = 10;
x = x + 5;  // x is now 15
x += 5;     // x is now 20
x -= 5;     // x is now 15
x *= 5;     // x is now 75
x /= 5;     // x is now 15
x %= 5;     // x is now 0
```
3.  **Comparison operators**: These operators are used to compare two values and return a boolean value indicating whether the comparison is true or false.
```javascript
console.log(10 > 5);   // Output: true
console.log(10 < 5);   // Output: false
console.log(10 >= 5);  // Output: true
console.log(10 <= 5);  // Output: false
console.log(10 == 5);  // Output: false
console.log(10 != 5);  // Output: true
```
4.  **Logical operators**: These operators are used to perform logical operations, such as AND, OR, and NOT.
```javascript
console.log(true && true);   // Output: true
console.log(true && false);  // Output: false
console.log(true || false);  // Output: true
console.log(!true);          // Output: false
```
5.  **Unary operators**: These operators perform an operation on a single operand.
```javascript
let x = 10;
console.log(typeof x);  // Output: "number"
console.log(+x);        // Output: 10
console.log(-x);        // Output: -10
console.log(++x);       // Output: 11
console.log(--x);       // Output: 10
```
6.  **Ternary operator**: This operator takes three operands and is used to perform different actions based on whether a condition is true or false.
```javascript
let x = 10;
let y = 5;
let max = (x > y) ? x : y;  // max is 10
```
7.  **Spread operator**: This operator is used to expand an iterable object into a list of values.
```javascript
let arr1 = [1, 2, 3];
let arr2 = [4, 5, 6];
let combined = [...arr1, ...arr2];  // combined is [1, 2, 3, 4, 5, 6]
/*************************************/
function sum(x, y, z) {
  return x + y + z;
}
let arr = [1, 2, 3];
console.log(sum(...arr));  // Output: 6
/*************************************/
let str = "hello";
let chars = [...str];  // chars is ['h', 'e', 'l', 'l', 'o']
```
8.  **Destructuring operator**: This operator is used to extract values from arrays or objects and assign them to variables.
```javascript
let arr = [1, 2, 3];
let [x, y, z] = arr;  // x is 1, y is 2, and z is 3
let obj = {a: 1, b: 2, c: 3};
let {a, b, c} = obj;  // a is 1, b is 2, and c is 3
```
9.  **Comma operator**: This operator is used to separate multiple expressions in a list.
```javascript
let x = 0, y = 1, z = 2;
```

---

## Wired JavaScript

### Reason #1 : dynamically-typed arrays
>In JavaScript, arrays are objects that are used to store a collection of values. Unlike in some other programming languages, the values in a JavaScript array do not have to be of the same type. This means that you can store a mixture of different types of values in a single array, such as numbers, strings, and boolean values.

>One reason why JavaScript arrays are able to store values of different types is because JavaScript is a dynamically-typed language. This means that the type of a value in JavaScript is determined at runtime, rather than being determined at compile-time as it is in some other languages. This allows for greater flexibility in the types of values that can be stored in an array, as well as in other data structures.

>Another reason is that arrays in JavaScript are implemented as objects, which are more flexible and versatile data structures than arrays in some other languages. This allows JavaScript arrays to have more capabilities and behaviors than arrays in some other languages.

### Reason #2 : type conversions
```javascript
console.log(1 + '2');  // Output: "12" (1 is coerced to a string)
console.log(true + 1);  // Output: 2 (true is coerced to a number)

console.log(Number('123'));  // Output: 123 (string is converted to a number)
console.log(String(123));    // Output: "123" (number is converted to a string)

let x = '123';
x = 123;  // x is now a number
```

### Reason #3 : bit more wiredness
| Value | Description|
| :---: |--- |
| `undefined` | Object's value is not defined. |
| `NaN` | Not a Number, the number is invalid. |
| `if (NaN != NaN)` | The expression `NaN != NaN` will always evaluate to `true`.This is because `NaN` (Not a Number) is a special value in JavaScript that represents a value that is not a number. `NaN` is the result of an operation that cannot produce a normal result, such as dividing 0 by 0 or trying to parse an invalid number from a string. Because `NaN` is not equal to any value, including itself, the expression `NaN != NaN` will always evaluate to `true`.You can use the `isNaN()` function to check whether a value is `NaN` | 
| `typeof`| gives you variable type. |
| `var number="678"; if (number == 678)` | `number == 678` is true!, JavaScript converted string to number if possbile. |
| `var number="biswajit"; if (number == NaN)` | `number == NaN` is false!, JavaScript converted string to number but "Biswajit" is NaN and NaN is not equal with NaN. |
| `1 == true` | true!, JavaScript converted `true` to number `1`. |
| `"1" == true` | true!, both get converted and `1 == 1` is true! |
| `undefined == null` | The expression `undefined == null` will evaluate to `true`. In JavaScript, both `undefined` and `null` represent the absence of a value. They are both used to indicate that a variable has been declared but has not been assigned a value. Because `undefined` and `null` represent the same concept, the equality operator `==` considers them to be equal. You can use the strict equality operator `===` to check whether a value is strictly equal to `undefined` or `null`, rather than just equal: |
| `false == ""` | true!, empty string converted into `0`, `0 == 0` is true. |
| `=== vs ==` | `===` is *(strict equality)* says don't make it complicated! don't do any convertion. `==` *(equality)* says make it complicated, do convertions. |
| `var sum = 3 + "4"` | `"34"` |
| `var multiply = 3 * "4"` | `12` |
| `var divide = 3 / "4"` | `0.7` |
| `var subtraction = 3 - "4"` | `-1` |
| `undefined` | `falsy!`|
| `null` | `falsy!` |
| `NaN` | `falsy!` |
| `0` | `falsy!` |
| `""` | `falsy!` |
