## Objects

>In JavaScript, an object is a collection of key-value pairs. The keys are strings (or symbols) and the values can be any type of data, including other objects. You can create an object using the object literal syntax, like this:
```javascript
let person = {
  name: "John",
  age: 30,
  job: "developer"
};
```
>You can access the properties of an object using dot notation or bracket notation:
```javascript
console.log(person.name);  // prints "John"
console.log(person["age"]);  // prints 30
```
>You can also add, modify, and delete properties of an object like this:
```javascript
person.city = "New York";
person.name = "Jane";
delete person.age;
```
>In addition to the key-value pairs, an object can also have methods, which are functions that belong to the object. You can define a method like this:
```javascript
let person = {
  name: "John",
  greet: function() {
    console.log("Hello, my name is " + this.name);
  }
};

person.greet();  // prints "Hello, my name is John"
```
>The `this` keyword refers to the object itself inside a method.

---

## Constructors

>A constructor is a special type of function that is used to create and initialize an object.

### Create & use constructors

#### Way #1
>In JavaScript, you can define a constructor function using the `class` syntax, or you can use the `Object` constructor to create an object.
>Here's an example of a constructor function using the `class` syntax:
```javascript
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  greet() {
    console.log("Hello, my name is " + this.name);
  }
}

let john = new Person("John", 30);
john.greet();  // prints "Hello, my name is John"
```
>The `constructor` function is called automatically when you use the `new` operator to create an object from the `Person` class. The `this` keyword inside the `constructor` function refers to the new object being created.

#### Way #2
>You can also use the `Object` constructor to create an object and define its properties and methods:
```javascript
let person = new Object();
person.name = "John";
person.age = 30;
person.greet = function() {
  console.log("Hello, my name is " + this.name);
};

person.greet();  // prints "Hello, my name is John"
```
>In this example, we use the `Object` constructor to create an empty object and then add properties and methods to it.
>Constructors are useful for creating objects with the same properties and methods, and they can also accept arguments to customize the objects they create.

#### Way #3
```javascript
function Person (name, height, age) {
	this.name = name;
	this.height = height;
	this.age = age;
	this.run = function() {
		console.log ("running...");
	}
}

let p1 = new Person("Biswajit", 5.3, 21);
let p2 = new Person("Avijit", 5.4, 17);
```
>In this example, `Person` is a constructor that is taking some arguments and assigning those arguments to an object's properties.
##### There is another way of passing argument to a constructor
```javascript
let biswajit = {
	name : "Biswajit Malakar",
	age : 21,
	height : 5.3
}

function Person (person) {
	this.name = person.name;
	this.height = person.height;
	this.age = person.age;
	this.run = function() {
		console.log ("running...");
	}
}

let p1 = new Person(biswajit);
```

### instanceof
>In JavaScript, the `instanceof` operator is used to determine whether an object is an instance of a particular constructor or constructor's prototype. It is typically used to check the type of an object at runtime.
```javascript
function Animal(name) {
  this.name = name;
}

const dog = new Animal('dog');

console.log(dog instanceof Animal); // Output: true
console.log(dog instanceof Object); // Output: true
```
>In the example above, the `instanceof` operator is used to check whether the `dog` object is an instance of the `Animal` constructor and the `Object` constructor. It returns `true` for both checks because the `dog` object was created using the `Animal` constructor, and all objects in JavaScript are instances of the `Object` constructor.

### constructed objects can have their own independent properties
```javascript
function Person(name, age, weight) {
	this.name = name;
	this.age = age;
	this.weight = weight;
	this.run = function () {
		console.log("running...");
	}
}

let p1 = new Person("Biswajit", 21, 5.3);
let p2 = new Person("Avijit", 17, 5.4);

// changing p1 object's properties
delete p1.age;
p1.hairStyle = "straight hair";

console.log(p2 instanceof Person); // Output : true
console.log(p1 instanceof Person); // Output : true
```

---

## JavaScript built-in objects

>JavaScript has a number of built-in objects that provide a range of functions and properties for working with numbers, dates, text, and other types of data. Here is a list of some of the most commonly used built-in objects in JavaScript:

-   `Object`: The base object for all other objects in JavaScript. It provides basic functions and properties for working with objects, such as the `constructor` property and the `toString()` method.

-   `Array`: An object for storing and manipulating collections of data. It provides methods for adding, removing, and accessing elements in the collection, as well as methods for sorting and searching the collection.

-   `String`: An object for representing and manipulating text. It provides methods for manipulating strings, such as the `charAt()` method for accessing individual characters in a string and the `concat()` method for concatenating two or more strings.

-   `Number`: An object for representing and manipulating numbers. It provides methods for converting numbers to and from strings, as well as methods for performing arithmetic operations on numbers.

-   `Date`: An object for representing and manipulating dates and times. It provides methods for manipulating and formatting dates and times, as well as methods for performing arithmetic operations on dates.

>These are just a few examples of the built-in objects available in JavaScript. There are many others, including the `Math` object for mathematical operations, the `Function` object for working with functions, and the `RegExp` object for working with regular expressions.

---

## Prototypes 

>In JavaScript, a prototype is an object that serves as a template for creating new objects. Every object in JavaScript has a prototype, which is used to define and inherit properties and methods.
```javascript
function Person(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}

Person.prototype.sayHello = function() {
  return "Hello, my name is " + this.firstName + " " + this.lastName;
};

var person1 = new Person("John", "Doe");
console.log(person1.sayHello()); // "Hello, my name is John Doe"

var person2 = new Person("Jane", "Doe");
console.log(person2.sayHello()); // "Hello, my name is Jane Doe"
```
>In this example, the `Person` function serves as a constructor for creating new `Person` objects. The `sayHello` method is added to the `Person.prototype` object, which means that it is available to all `Person` objects. When we create new `Person` objects using the `new` keyword, they inherit the properties and methods from the `Person.prototype` object.

### Why the hack we need prototype ?

There are a few reasons why prototypes are useful in JavaScript:

1.  They allow you to define methods and properties on an object that can be inherited by all instances of that object, rather than having to define the same methods and properties on each individual instance. This can save a lot of time and code duplication.

2.  Prototypes allow you to create a hierarchy of objects, with each object inheriting from a parent object. This is known as prototypical inheritance, and it can be a powerful way to organize your code and reuse common functionality across different objects.
   
3.  Prototypes can be modified at runtime, which allows you to add or modify methods and properties on objects after they have been created. This can be useful for adding or changing behavior on the fly.  

Overall, prototypes are a fundamental part of JavaScript and are an important tool for creating efficient, reusable code.