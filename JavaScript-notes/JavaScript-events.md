>Event handling is a mechanism in JavaScript that allows you to specify code to be executed when a particular event occurs in the Document Object Model (DOM). An event is a signal that something has happened in the system, such as a mouse click, a key press, or a page load.

>There are several ways to handle events in JavaScript, but the most common way is to use event listeners. An event listener is a function that is registered to be executed when a particular event occurs.

>Here is a list of some common event listeners that you can use in JavaScript:

-  **click** :
>The `click` event is a mouse event in JavaScript that is triggered when an element is clicked. A click is defined as a mouse down and mouse up action on a single element, or a series of mouse down and mouse up actions on a single element within a short period of time.
>The `click` event is a widely used event in JavaScript, and it is supported by most HTML elements. You can use the `click` event to specify code to be executed when an element is clicked.
```javascript
<button id="myButton">Click me</button>

<script>
  let button = document.getElementById('myButton');
  button.addEventListener('click', function() {
    console.log('Button was clicked');
  });
</script>
```

-   **dblclick**:
>The `dblclick` event is a mouse event in JavaScript that is triggered when an element is double-clicked. A double-click is defined as two clicks on a single element within a short period of time.
>The `dblclick` event is a widely used event in JavaScript, and it is supported by most HTML elements. You can use the `dblclick` event to specify code to be executed when an element is double-clicked.
```javascript
<button id="myButton">Double-click me</button>

<script>
  let button = document.getElementById('myButton');
  button.addEventListener('dblclick', function() {
    console.log('Button was double-clicked');
  });
</script>
```

-   **mousedown**:
>The `mousedown` event is a mouse event in JavaScript that is triggered when a mouse button is pressed down on an element. The `mousedown` event is a low-level event, and it is triggered before the `click` and `mouseup` events.
>The `mousedown` event is a widely used event in JavaScript, and it is supported by most HTML elements. You can use the `mousedown` event to specify code to be executed when a mouse button is pressed down on an element.
```javascript
<button id="myButton">Click me</button>

<script>
  let button = document.getElementById('myButton');
  button.addEventListener('mousedown', function() {
    console.log('Mouse button was pressed down on the button');
  });
</script>
```

-   **mouseup**:
>The `mouseup` event is a mouse event in JavaScript that is triggered when a mouse button is released on an element. The `mouseup` event is a low-level event, and it is triggered after the `mousedown` event and before the `click` event.
>The `mouseup` event is a widely used event in JavaScript, and it is supported by most HTML elements. You can use the `mouseup` event to specify code to be executed when a mouse button is released on an element.
```javascript
<button id="myButton">Click me</button>

<script>
  let button = document.getElementById('myButton');
  button.addEventListener('mouseup', function() {
    console.log('Mouse button was released on the button');
  });
</script>
```

-   **mouseenter**:
>The `mouseenter` event is a mouse event in JavaScript that is triggered when the mouse pointer enters an element. The `mouseenter` event is a low-level event, and it is triggered before the `mouseover` event.
>The `mouseenter` event is supported by most HTML elements, and it is a useful event for creating interactive web pages and applications. You can use the `mouseenter` event to specify code to be executed when the mouse pointer enters an element.
```javascript
<div id="myDiv">Hover over me</div>

<script>
  let div = document.getElementById('myDiv');
  div.addEventListener('mouseenter', function() {
    console.log('Mouse entered the div');
  });
</script>
```

-   **mouseleave**:
>The `mouseleave` event is a mouse event in JavaScript that is triggered when the mouse pointer leaves an element. The `mouseleave` event is a low-level event, and it is triggered after the `mouseout` event.
>The `mouseleave` event is supported by most HTML elements, and it is a useful event for creating interactive web pages and applications. You can use the `mouseleave` event to specify code to be executed when the mouse pointer leaves an element.
```javascript
<div id="myDiv">Hover over me</div>

<script>
  let div = document.getElementById('myDiv');
  div.addEventListener('mouseleave', function() {
    console.log('Mouse left the div');
  });
</script>
```

-   **mousemove**:
>The `mousemove` event is a mouse event in JavaScript that is triggered when the mouse pointer is moved over an element. The `mousemove` event is a low-level event, and it is triggered repeatedly while the mouse pointer is being moved over an element.
>The `mousemove` event is supported by most HTML elements, and it is a useful event for creating interactive web pages and applications. You can use the `mousemove` event to specify code to be executed when the mouse pointer is moved over an element.
```javascript
<div id="myDiv">Move the mouse over me</div>

<script>
  let div = document.getElementById('myDiv');
  div.addEventListener('mousemove', function(event) {
    console.log(`Mouse pointer is at (${event.clientX}, ${event.clientY})`);
  });
</script>
```

-   **keydown**:
>The `keydown` event is a type of event that occurs when a user presses a key on the keyboard. It is fired just before the key is released. This event can be used to trigger an action in a web page or application when a user presses a specific key. You can use the `addEventListener()` method to attach a `keydown` event to an element on a web page.
```javascript
document.addEventListener('keydown', function(event) {
  console.log('Keydown event: ', event);
});
```
-   **keyup**:
>The `keyup` event is a type of event that occurs when a user releases a key on the keyboard. It is fired just after the key is released. This event can be used to trigger an action in a web page or application when a user releases a specific key. You can use the `addEventListener()` method to attach a `keyup` event to an element on a web page.
```javascript
document.addEventListener('keyup', function(event) {
  console.log('Keyup event: ', event);
});
```

-   **keypress**:
>The `keypress` event is a type of event that occurs when a user presses and releases a key on the keyboard. It is fired after the `keydown` event and before the `keyup` event. This event can be used to trigger an action in a web page or application when a user presses and releases a specific key. You can use the `addEventListener()` method to attach a `keypress` event to an element on a web page.
```javascript
document.addEventListener('keypress', function(event) {
  console.log('Keypress event: ', event);
});
```

-   **focus**: 
>The `focus` event is a type of event that occurs when an element on a web page receives focus. This can happen when a user clicks on an element with the mouse, uses the tab key to navigate to the element, or programmatically sets focus to the element using JavaScript.
>You can use the `addEventListener()` method to attach a `focus` event to an element on a web page. Here is an example of how to use the `focus` event in JavaScript:
```javascript
document.getElementById('myInput').addEventListener('focus', function(event) {
  console.log('Input element has focus');
});
```

-   **blur**:
>The `blur` event is a type of event that occurs when an element on a web page loses focus. This can happen when a user clicks on another element with the mouse, uses the tab key to navigate to another element, or programmatically sets focus to another element using JavaScript.
>You can use the `addEventListener()` method to attach a `blur` event to an element on a web page. Here is an example of how to use the `blur` event in JavaScript:
```javascript
document.getElementById('myInput').addEventListener('blur', function(event) {
  console.log('Input element has lost focus');
});
```

-   **submit**: 
>The `submit` event is a type of event that occurs when a user submits a form. This can happen when a user clicks on the submit button of a form, or when a user presses the `Enter` key while an input field in the form has focus.
>You can use the `addEventListener()` method to attach a `submit` event to a form element on a web page. Here is an example of how to use the `submit` event in JavaScript:
```javascript
document.getElementById('myForm').addEventListener('submit', function(event) {
  event.preventDefault();
  console.log('Form submitted');
});
```

-   **change**:
>The `change` event is a type of event that occurs when the value of an element on a web page has changed. This event is commonly used with form elements such as input fields, select boxes, and textarea elements.
>You can use the `addEventListener()` method to attach a `change` event to an element on a web page. Here is an example of how to use the `change` event in JavaScript:
```javascript
document.getElementById('myInput').addEventListener('change', function(event) {
  console.log('Input value has changed');
});
```

-   **resize**:
>The `resize` event is a type of event that occurs when the size of a window or an element on a web page has been changed. This event is commonly used to detect when the size of the browser window has been changed, or when the size of an element on the page has been changed through JavaScript.
>You can use the `addEventListener()` method to attach a `resize` event to the `window` object or to an element on a web page. Here is an example of how to use the `resize` event to detect when the size of the browser window has been changed:
```javascript
window.addEventListener('resize', function(event) {
  console.log('Window size has been changed');
});
```

-   **scroll**:
>The `scroll` event is a type of event that occurs when a user scrolls through a web page. This event is commonly used to detect when a user has scrolled to a specific section of a page, or to trigger an action when the user has scrolled to the bottom of a page.
>You can use the `addEventListener()` method to attach a `scroll` event to the `window` object or to an element on a web page. Here is an example of how to use the `scroll` event to detect when a user has scrolled to the bottom of a page:
```javascript
window.addEventListener('scroll', function(event) {
  if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
    console.log('You have reached the bottom of the page');
  }
});
```

-   **select**:
>The `select` event is a type of event that occurs when a user selects text in an input field or a textarea element. This event is fired when the user finishes selecting text and releases the mouse button or the `Shift` key.
>You can use the `addEventListener()` method to attach a `select` event to an input field or a textarea element on a web page. Here is an example of how to use the `select` event in JavaScript:
```javascript
document.getElementById('myInput').addEventListener('select', function(event) {
  console.log('Text has been selected');
});
```
-   **load**:
>The `load` event is a type of event that occurs when a web page or a resource has finished loading. This event is commonly used to detect when a web page has finished loading, or when a specific image or script has finished loading.
>You can use the `addEventListener()` method to attach a `load` event to the `window` object or to an element on a web page. Here is an example of how to use the `load` event to detect when a web page has finished loading:
```javascript
window.addEventListener('load', function(event) {
  console.log('Page has finished loading');
});
```

-   **unload**: 
>The `unload` event is a type of event that occurs when a web page or a frame is being unloaded. This event is commonly used to detect when a user is leaving a web page, or to trigger an action when a user closes a web page or navigates away from it.
>You can use the `addEventListener()` method to attach an `unload` event to the `window` object or to an element on a web page. Here is an example of how to use the `unload` event to detect when a user is leaving a web page:
```javascript
window.addEventListener('unload', function(event) {
  console.log('Page is being unloaded');
});
```

-   **beforeunload**:
>The `beforeunload` event is a type of event that occurs when a web page is about to be unloaded. This event is commonly used to detect when a user is leaving a web page, or to trigger an action when a user closes a web page or navigates away from it.
>You can use the `addEventListener()` method to attach a `beforeunload` event to the `window` object. Here is an example of how to use the `beforeunload` event to detect when a user is leaving a web page:
```javascript
window.addEventListener('beforeunload', function(event) {
  console.log('Page is about to be unloaded');
});
```

-   **error**:
>The `error` event is a type of event that occurs when an error occurs while loading a resource on a web page. This event is commonly used to handle errors that occur when loading images, scripts, or other resources on a web page.
>You can use the `addEventListener()` method to attach an `error` event to an element on a web page. Here is an example of how to use the `error` event to detect when an image fails to load:
```javascript
document.getElementById('myImage').addEventListener('error', function(event) {
  console.log('Error loading image');
});
```