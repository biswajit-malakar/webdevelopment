
## Function declaration & Function expression

>In JavaScript, there are two main ways to define a function: *function declarations* and *function expressions*.

>*Function declarations* are made using the `function` keyword, followed by the function name, a list of parameters in parentheses, and a block of code that makes up the function body.
>Here's an example of a function declaration:
```javascript
function greet(name) {
  console.log(`Hello, ${name}!`);
}

greet('John'); // Output: "Hello, John!"
```

>*Function expressions*, on the other hand, are created by assigning a function to a variable. There are two main types of function expressions: anonymous function expressions and named function expressions.
>*Anonymous function expressions* are function expressions that don't have a name, like this:
```javascript
const greet = function(name) {
  console.log(`Hello, ${name}!`);
}

greet('John'); // Output: "Hello, John!"
```
>*Named function expressions* are function expressions that have a name, which can be used inside the function body to refer to itself.
>Here's an example of a named function expression:
```javascript
const greet = function greet(name) {
  console.log(`Hello, ${name}!`);
}

greet('John'); // Output: "Hello, John!"
```

>*Function declarations are hoisted*, which means that they are moved to the top of the code before it is executed. This means that you can use a function before you declare it in your code. *Function expressions are not hoisted*, so you need to make sure that you declare a function expression before you try to use it.
```javascript
greet('John'); // Output: "Hello, John!"

function greet(name) {
  console.log(`Hello, ${name}!`);
}
```

```javascript
greet('John'); // Uncaught TypeError: greet is not a function

const greet = function(name) {
  console.log(`Hello, ${name}!`);
}
```

---

## Anonymous function

>Anonymous functions are functions that are not given a name. They are often used as function arguments or as the return value of another function.
>Here's an example of an anonymous function being passed as an argument to the `setTimeout` function, which calls the function after a specified number of milliseconds:
```javascript
setTimeout(function() {
  console.log('Hello, world!');
}, 1000); // Output: "Hello, world!" after 1 second
```
>Anonymous functions can also be assigned to variables and stored in data structures. Here's an example of an anonymous function being assigned to a variable and then called:
```javascript
const greet = function(name) {
  console.log(`Hello, ${name}!`);
};

greet('John'); // Output: "Hello, John!"
```

---

## Closure

>In JavaScript, functions create their own scope. This means that variables defined inside a function are not accessible from outside the function. However, if a function returns another function that has a reference to a variable defined in the outer function, the returned function has access to that variable, even after the outer function has completed execution. This is known as a closure.
>Here's an example of how a closure works:
```javascript
function outerFunction(x) {
  var outerVariable = x;

  return function innerFunction() {
    console.log(outerVariable);
  }
}

var inner = outerFunction("Hello, World!");
inner();  // prints "Hello, World!"
```
>In this example, `outerFunction` is the outer function and `innerFunction` is the inner function. The inner function has a reference to the `outerVariable` variable defined in the outer function, so it can access the variable even after the outer function has completed execution. When we call `inner()`, it prints `"Hello, World!"` to the console.

>Closures are useful for creating private variables and functions that can only be accessed and modified by a specific function or object. For example, you can use a closure to create an object with a private property that can only be modified by a setter function:
```javascript
function createCounter() {
  var count = 0;

  return {
    increment: function() {
      count++;
    },
    getCount: function() {
      return count;
    }
  }
}

var counter = createCounter();
console.log(counter.getCount());  // prints 0
counter.increment();
console.log(counter.getCount());  // prints 1
```
>In this example, the `count` variable is a private property of the object returned by `createCounter()`. It can only be modified by calling the `increment()` function, which is a closure that has access to the `count` variable. The `getCount()` function is also a closure that has access to the `count` variable, so it can return the current value of the `count` variable.